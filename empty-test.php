<?php
$dbname = $_POST['db_name'];
$dbuser = $_POST['db_user'];
$dbpass = $_POST['db_pass'];
$username = $_POST['username'];
$password = $_POST['password'];

if ($dbname == "" || $dbuser == "" || $dbpass == "" || $username == "" || $password ==
    "") {
    echo ("Missing Information!");
    var_dump($_POST);
} else {
    echo ("Success!");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Rikudo Sennin">

    <title>Untitled</title>
    <style type="text/css">
        form.c1 {padding-left: 50px}
    </style>
</head>

<body>
    <form method="post" class="c1">
        <h2>Database connection settings</h2>
        <label for="db_name">Database name:</label> <input type="text" id="db_name" name="db_name">
        <br>
        <label for="db_user">Database username:</label> <input type="text" id="db_user" name="db_user">
        <br>
        <label for="db_pass">Database password:</label> <input type="password" id="db_pass" name="db_pass">
        <br>

        <h2>CPanel settings</h2><label for="username">Username:</label> <input type="text" id="username" name="username">
        <br>
        <label for="password">Password:</label> <input type="password" id="password" name="password">
        <br>
        <input type="submit" name="submit_install" value="Install">
    </form>
</body>
</html>
