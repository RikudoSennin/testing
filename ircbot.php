<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dor
 * Date: 09/03/12
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 *
 * @package IRC Bot
 */

set_time_limit(0); //Don't kill the script after a set time.
header('Content-type: text/plain'); //Easier display of the log. Maybe in the future log will look better.
error_reporting(E_ALL); //Show any error the pops up.

/* -- Disable output buffering -- */
ob_implicit_flush(true);
ob_end_flush();

/**
 * Defines global variables and server stuff.
 */

class Server {

    /**
     * @var String holds the server address.
     */
    public static $address = "localhost";
    /**
     * @var Integer holds the server port
     */
    public static $port = 6667;
    /**
     * @var String holds the server password, if exists.
     */
    public static $password = "";
    /**
     * @var /Bot holds the Bot object.
     */
    public static $bot;

    /**
     * @static
     * Connect to the server and place the bot object in a variable.
     */
    public static function connect() {
        $config    = array(
            "mNick" => "MyIRCBot",
            "aNick" => "MyIRCBot2",
            "rName" => "The best IRC bot out there."
        );
        self::$bot = new Bot(self::$address, self::$port, self::$password, $config);
    }
}

/**
 * Defines the Bot object.
 */
class Bot {

    /**
     * @var Resource holds server connection
     */
    protected $con;

    /**
     * @var String main nickname.
     */
    protected $mNick;
    /**
     * @var String alternative nickname. Will be used in case main nick is taken.
     */
    protected $aNick;

    /**
     * @var String the data being read right now
     */
    protected $data;

    /**
     * @var Array configuration array, will hold various variables.
     */
    protected $config;

    /**
     * @var User[] global user list
     */
    protected $userList;
    /**
     * @var Channel[] channel list
     */
    protected $chanList;

    public function __construct($address, $port, $password, $config) {
        #First, let's take care of the configuration

        //Iterate the config array.
        foreach ($config as $key => $value) {
            //If the property exists in this class,
            if (property_exists($this, $key)) {
                //Set it.
                $this->$key = $value;
            }
            else {
                //If not, place it in the config.
                $this->config[$key] = $value;
            }
        }

        #Now connect
        $this->con = fsockopen($address, $port, $errNo, $errStr);
        if (!$this->con) {
            throw new Exception("Connection Failed! ($errNo) $errStr");
        }

        #Connection established. Send in connection headers
        if (!empty($password)) {
            $this->send("PASS $password");
        }
        $this->send("USER $this->mNick 0 * :{$this->config["rName"]}");
        $this->send("NICK $this->mNick");

        $this->main();

    }

    /**
     * Output a line to the visible log
     *
     * @param $origin string Origin of message
     * @param $data   string Data to output
     */
    public function output($origin, $data) {
        echo date("H:i:s") . " " . $origin . ": " . $data . "\n";
    }

    /**
     * Send a line of data to the server
     *
     * @param $data string Data to send
     */
    protected function send($data) {
        fwrite($this->con, $data . "\r\n");
        $this->output("SEND", $data);
    }

    /**
     * Get a line from the server
     */

    protected function get() {
        $this->data = fgets($this->con); //Read
        $this->data = trim($this->data); //Trim
        $this->output("GET", $this->data); //Output
    }

    /**
     * Main function
     * Responsible for all of the Bot's activity.
     */

    protected function main() {
        //While data keeps coming
        while (!feof($this->con)) {
            //Read a line
            $this->get();
            //If it's empty, finish the iteration and continue with the next.
            if (empty($this->data)) {
                continue;
            }

            #We now have a none-empty line
            //Split it so we can follow it easily.
            $data = explode(" ", $this->data);

            //If the first word is "PING"
            if ($data[0] == "PING") {
                //Reply with a "PONG" to keep connection alive
                $this->send("PONG " . $data[1]);
            }

        }
    }

    /**
     * Join a channel
     *
     * @param Channel $channel
     */
    protected function join(Channel $channel) {

    }

    /**
     * Add $user to $channel.
     * @param Channel $channel
     * @param User    $user
     *
     * @throws Exception
     */
    protected function addUser(Channel $channel, User $user) {
        if (!isset($this->chanList[$channel->name])) {          #Channel is not on channel list
            throw new Exception("You are not on that channel");
        }
        if (!isset($this->userList[$user->nick])) {             #User is not on global user list (New user)
            $this->userList[$user->nick] = $user;               #Add user to list
        }
        $this->chanList[$channel->name]->addUser($user);        #Add the user to the channel user list
    }
}

/**
 * Channel object
 */
class Channel {

    /**
     * @var String $name Channel name, starting with a #.
     */
    public $name;
    /**
     * @var Array Array of User objects.
     */
    protected $userList;

    /**
     * Assign channel name
     *
     * @param $name
     */
    public function __construct($name) {
        if (!strpos($name, "#") === 0) {    #If channel name does not begin with #, correct it.
            $name = "#" . $name;
        }
        $this->name = $name;                #Assign to object property.
    }

    /**
     * @return String Returns channel name when object is accessed as a string
     */
    public function __toString() {
        return $this->name;
    }

    /**
     * Add a user to the channel user list.
     *
     * @param User $user
     */
    public function addUser(User &$user) {              #Note that passing by reference, we want the SAME OBJECT FROM THE GLOBAL USER LIST
        if (!isset($this->userList[$user->nick])) {
            $this->userList[$user->nick] = $user;
        }
    }
}

Server::connect();