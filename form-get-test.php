<?php
    
    $in_database = array('John', 'David', 'Eve');
    $names = array('John', 'Jim', 'David', 'Dianne', 'Eve');
    $result = "";
    
    foreach ($names as $name) {
        $result .= "<label for=\"checkbox-{$name}\">{$name} ";
        $result .= "<input type=\"checkbox\"";
        if (in_array($name,$in_database)) {
            $result .= " checked=\"checked\"";
        }
        $result .= "></label>";
    }
    
    echo $result;
    
?>