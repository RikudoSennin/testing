<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>

    <style type="text/css">
            /* Compressed Oval Animation */

        #oval {
            background:        url(http://www.hdwallpapersarena.com/wp-content/uploads/2012/05/sunset_landscape.jpg);
            height:            300px;
            width:             300px;
            margin:            20px;
            border-radius:     150px;
            -webkit-animation: wobbling 2s infinite;
            box-shadow:        0 5px 5px rgba(0, 0, 0, 0.5), inset 0 50px 50px rgba(255,255,255,0.5);
            border: 5px solid black;
        }

        @-webkit-keyframes wobbling {
            0%   {  }
            75%  {
                height:              200px;
                border-radius:       150px / 100px;
                margin-top:          70px;
                background-position: 0 -150px;
                box-shadow:          0 30px 30px 0 rgba(0, 0, 0, 0.5),
                                     inset 0 10px 20px rgba(255,255,255,0.5);
                border-width:        2px 5px;
            }
            100% {  }
        }

    </style>
</head>
<body>

<div id="oval"></div>

</body>
</html>