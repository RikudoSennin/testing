<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="author" content="Rikudo Sennin" />
	
        <title>Untitled</title>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script>
            function createCookie(name,value,days) {
            	if (days) {
            		var date = new Date();
            		date.setTime(date.getTime()+(days*24*60*60*1000));
            		var expires = "; expires="+date.toGMTString();
            	}
            	else var expires = "";
            	document.cookie = name+"="+value+expires+"; path=/";
            }

            $(function() {
                $("#main").submit(function() {
                    value = $('input[name="foo"]', this).val();
                    createCookie('pass', value, 365);

                });
            });
            
            
        </script>
    </head>
	
    <body>
    
        <form id="main" method=POST>
            <input type="password" name="foo">
            <button type="submit">Go!</button>
        </form>
    
    </body>
</html>