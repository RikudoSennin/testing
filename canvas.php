<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="author" content="Rikudo Sennin" />
	
        <title>Untitled</title>
        
        <style>
            * { padding: 0; margin: 0; }
        </style>
        
    </head>
	
    <body>
    
        <canvas id="myCanvas" width="500" height="500">
            Your browser does not support canvas.
        </canvas>
    
    </body>
    
    <script>
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        
        ctx.beginPath();
        ctx.moveTo(50,50);
        ctx.lineTo(50,150);
        ctx.lineTo(150,150);
        ctx.closePath();
        ctx.fillStyle = "rgba(255,0,0,0.5)";
        ctx.fill();
        ctx.stroke();
    </script>
</html>