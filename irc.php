<?php
    
    header("Content-type: text/plain");
    error_reporting(E_ALL);
    
    set_time_limit(0);
    
    class irc {
        
        protected $cnick; //Current Nick
        protected $mnick; //Main Nick
        protected $anick; //Alternative Nick
        
        protected $rname = "Rikudo Bot"; //Real Name
        
        protected $server; //Server name
        protected $port = 6667; //Server port
        protected $spass; //Server password
        protected $ajoin = "#RikudoBot,#botters"; //Channels to auto join
        
        protected $connected = false; //Connection status.
        
        protected $conn; //Will hold the server connection.
        protected $message; //Will hold the current message from the server.
        
        protected $nicklist; //Global nicklist for all channels
        /**
         * $nicklist structure:
         * $nicklist["Channel"]["nick"]["level"] = array("+","@",etc);
         */
        
        protected $admins = array("RikudoSennin"); //An array containing all the admin nicks.
        
        protected $isop_checking = false;
        
        protected $dead_count = 0;
        
        public function __construct($server, $nick, $port = 6667, $spass = "") {
            $this->server = $server;
            $this->mnick = $nick;
            $this->cnick = $nick;
            $this->anick = $nick."_";
            $this->conn = fsockopen($server,$port,$errno,$errstr);
            stream_set_timeout($this->conn,60);
            if (!$this->conn) {
                trigger_error("Could not complete connection. Error returned: ($errno) $errstr",E_USER_ERROR);
            }
            $this->login();
            $this->main(); 
            
        }
        
        protected function login() {
            //$this->send_data("$this->server CAP LS");
            if (!empty($this->spass)) $this->send_data("PASS $this->spass");
            $this->send_data("USER $this->mnick 0 * :$this->rname");
            $this->send_data("NICK $this->mnick");
            //$this->send_data("$this->server CAP REQ :multi-prefix");
        }
        
        protected function send_data($message) {
            fputs($this->conn,$message."\r\n");
            $this->output("out", $message."\r\n");
        }
        
        protected function get_data() {
            $this->message = fgets($this->conn);
            $this->output("in", $this->message);
        }
        
        protected function main() {
            if (feof($this->conn)) {
                $this->send_data("QUIT :Died");
            }
            $this->get_data();
            if (!empty($this->message)) {
                $this->message = str_replace("|[\r\n]|","",$this->message);
                $this->dead_count = 0;
                $message = explode(" ",$this->message);
            
                if ($message[0] == "PING") {
                    $this->send_data("PONG ". $message[1]);
                }
                if ($message[1] == "433") { //Nick already in use.
                    $this->send_data("NICK $this->anick");
                    $this->cnick == $this->anick;
                }
                if ($message[1] == "376") { //End of MOTD
                    $this->send_data("JOIN $this->ajoin");
                }
                
                if ($message[1] == "PRIVMSG") {
                    #$this->output("debug", "PRIVMSG");
                    $nick = $this->get_nick($message[0]);
                    #$this->output("debug", $nick);
                    if (in_array($nick,$this->admins)) { //This is an admin's message.
                        if ($message[3] == ":@cmd") {
                            $command = implode(" ",array_slice($message,4));
                            #$this->output("cmd",$command);
                            $command = eval("\$command = \"$command\";");
                            $this->send_data($command);
                        }
                    }
                }
                
                if ($message[1] == "353") { //NAMES
                    $i = 5;
                    while (isset($message[$i])) {
                        $nick = str_replace(":","",$message[$i]);
                        $modes = preg_match("|[@\+%&]|",$nick,$matches);
                        if ($modes == 1) {
                            $modes = array($matches[0]);
                            $nick = substr($nick,1);
                        }
                        else {
                            $modes = array();
                        }
                        $this->nicklist[$message[4]][$nick] = $modes;
                        $i++;
                    }
                }
                
                if ($message[1] == "366") { //END of NAMES
                    print_r($this->nicklist);
                }
                
                if ($message[1] == "MODE") {
                    if (preg_match("|^#|",$message[2]))
                        $this->send_data("NAMES {$message[2]}");
                }
                
                if ($message[1] == "JOIN") {
                    $nick = $this->get_nick($message[0]);
                    $chan = substr($message[2],2);
                    if (in_array($nick,$this->admins) && ($this->isop($this->cnick, $chan))) {
                        $this->send_data("MODE $chan +o $nick");
                    }
                }
                
                $this->main();
            }
            /*else {
                $this->dead_count++;
                if ($this->dead_count == 10) {
                    die("Connection Died at ". date("H:i:s"));
                }
            }*/
        }
        
        protected function isop($nick, $chan) {
            return ($this->nicklist[$chan][$nick][0] == "@");
        }
        
        protected function output($target = "", $message = "") {
            switch ($target) {
                case "out":
                    echo date("H:i:s"). " -> $message\n";
                    break;
                case "in":
                    echo date("H:i:s"). " <- $message\n";
                    break;
                case "cmd":
                    echo date("H:i:s"). " CMD: $message\n";
                    break;
                default:
                    echo date("H:i:s"). " $target: $message\n";
            }
            flush();
        }
        
        protected function get_nick($full_address) {
            if (preg_match("%:(.+)!%",$full_address,$matches) != 1) {
                trigger_error("Nickname not recognized.");
            }
            $result = $matches[1];
            return $result;
        }
    }
    
    $irc = new irc("chat.freenode.net","RikudoBot");
    
?>