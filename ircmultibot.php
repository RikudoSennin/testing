<?php

/**
 * PHP Based IRC Mutli-Bot
 *
 * This script will allow you to run multiple IRC bots on a server.
 *
 * IRC bots have a linked field of vision and information.
 *
 * The bots can assist each other in channel operation in case of floods
 * (by OPing each other and kicking/devoicing offenders synchronously)
 *
 * @package Tests
 */
set_time_limit(0);
/**
 * @const DEV indicates whether we are in developement mode
 */
define("DEV", true);
header("Content-Type: text/plain");
if (DEV) {
    error_reporting(E_ALL);
} //Show any error the pops up.

/* -- Disable output buffering -- */
ob_implicit_flush(true);
ob_end_flush();

/**
 * This class will act as the bot master
 * Coordinating the bots and keeping all the information
 * This classs is entirely static, meaning no instance properties or methods.
 *
 * Gedo is a term in Naruto where a single body can control 6 others (with a linked set of vision)
 *
 * @package    Tests
 * @subpackage Classes
 */
class Gedo {

    /**
     * @var array Holds the global user list for all bots to use
     */
    private static $userList;

    /**
     * @var array Holds global variables available for all application
     */
    private static $registry = array();

    /**
     * @static
     *
     * @param array $array
     *
     * This function takes an associative array and adds it into the registry.
     */

    private static $botList;

    public static function putRegistryArray($array) {
        self::$registry = array_merge(self::$registry, $array);
    }

    /**
     * @static
     *
     * @param string $key
     * @param mixed  $value
     *
     * This method takes a key/value pair and adds them into the registry.
     */
    public static function putRegistry($key, $value) {
        self::$registry[$key] = $value;
    }

    /**
     * @static
     *
     * @param string $key
     *
     * @return mixed
     *
     * This method takes a key and returns the value from the registry.
     */
    public static function getRegistry($key) {
        if (empty(self::$registry[$key])) {
            return false;
        }
        return self::$registry[$key];
    }

    public static function addBot($conf) {
        self::$botList[] = new Bot($conf);
    }

}

/**
 * This class defines the Bot object.
 *
 * Handles the connection and actual operations, and takes commands from the Gedo class.
 */
class Bot {

    /**
     * @var resource Holds the connection
     */
    private $con;
    /**
     * @var string Data being read right now
     */
    private $data = '';
    /**
     * @var array Bot specific configuration
     */
    private $conf = array();
    /**
     * @var array Holds channel objects which the bot is on
     */
    private $chanList = array();
    /**
     * @var string Holds current nickname of the bot
     */
    public $me = '';

    /**
     * @param array $conf Bot specific configuration
     */
    public function __construct($conf = array()) {
        $this->conf = $conf;
        $this->connect();
    }

    /**
     * @param $key
     * @param $value
     *
     * Add/modify a conf entry
     */
    public function __set($key, $value) {
        $this->conf[$key] = $value;
    }

    /**
     * @param $key
     *
     * @return mixed
     *
     * Return a conf entry
     */
    public function __get($key) {
        return $this->conf[$key];
    }

    /**
     * @param string $type Read, Write or Debug
     * @param string $line The line being echoed
     *
     * Print out a debug line, consisting of the current time, type of message, and the line itself.
     */
    private function debug($type = "", $line = "") {
        echo date("H:i:s") . " " . strtoupper($type) . " $line\r\n";
    }

    /**
     * @param string $data
     *
     * Write data to the connection
     */
    private function write($data = "") {
        fwrite($this->con, $data . "\r\n");
        $this->debug("write", $data);

    }

    /**
     * Read data from the connection and store it in $this->data.
     */
    private function read() {
        $data = fgets($this->con);
        $this->data = trim($data);
        $this->debug("read", $this->data);
    }

    /**
     * @throws Exception when connection fails
     *
     * Connects to the IRC server set by the Gedo class.
     */
    private function connect() {
        $this->con = fsockopen(Gedo::getRegistry("host"), Gedo::getRegistry("port"), $errno, $errstr);
        if (!$this->con) {
            throw new Exception("Connection Failed! ($errno) $errstr");
        }
        if ($pass = Gedo::getRegistry("pass")) {
            $this->write("PASS $pass");
        }
        $this->write("USER {$this->conf["mnick"]} 0 * :{$this->conf["rname"]}");
        $this->write("NICK {$this->conf["mnick"]}");

        $this->main();
    }

    /**
     * Main function, runs all the time in a loop while the connection is alive.
     * This method handles all of the bot's actions.
     */
    private function main() {
        while (!feof($this->con)) {
            $this->read();
            if ($this->data == "") {
                continue;
            }
            $data = explode(" ", $this->data);
            if (preg_match("|dump (.+)|", $this->data, $matches) && DEV) {
                $cmd = trim($matches[1], "()");
                var_dump($this->$cmd);
            }
            #Ping Pong with the server to keep the connection alive
            if ($data[0] == "PING") {
                $this->write("PONG $data[1]");
            }
            if ($data[1] == "001") {
                $this->me = $data[2];
            }
            //Todo: Have server variables saved on Gedo class.

            if ($data[1] == "376" || $data[1] == "422") { //MOTD file end or MOTD file missing
                $this->connectionEstablished();
            }

            if ($data[1] == "JOIN") {
                $chan = trim($data[2], ":");
                $nick = $this->parseNick($data[0]);
                if ($nick == $this->me) {
                    $this->chanList[$chan] = new Channel($chan);
                }
            }
        }
    }

    /**
     * @param string $address
     *
     * @return string
     *
     * Return a nickname from a given full address.
     */
    private function parseNick($address = "") {
        return preg_replace("|:?(.+)!.+@.+|", "$1", $address);
    }

    /**
     * @param string $channel
     *
     * @throws Exception when $channel is not a valid channel name.
     *
     * Ask the server to join a channel. Confirmation will be on the main() method.
     */
    private function join($channel = "") {
        if (!preg_match("|^#|", $channel)) {
            throw new Exception("Channel names must begin with a #");
        }
        if (!isset($this->chanList[$channel])) {
            $this->write("JOIN $channel");
            //Wait for server confirmation that we actually joined...
        }
    }

    /**
     * This method will run when connection to the server is fully established.
     */
    private function connectionEstablished() {
        $this->join("#Welcome");
    }

}

/**
 * Defines the Channel object.
 */
class Channel {

    /**
     * @var string Holds the channel name
     */
    public $name;

    public function __construct($name) {
        $this->name = $name;
    }

}

/**
 * Defines the User object.
 */
class User {

}

//Begin!

$registry = array(
    "host" => "localhost",
    "port" => 6667
);
Gedo::putRegistryArray($registry);
Gedo::addBot(array(
                  "mnick" => "Tendo",
                  "rnick" => "Surado",
                  "rname" => "God Realm"
             ));