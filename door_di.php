<?php

/**
 * This example file illustrates the use of Dependency Injection
 *
 * It demonstrates the correct construction of a house.
 *
 * @package Truth's playground
 */

/**
 * House
 *
 * Class to describe the house object.
 */
class House {
    private $door;

    public function __construct(Door $door) {
        $this->door = $door;
    }
}

/**
 * Door
 *
 * Class to describe the door object.
 */
class Door {

    private $doorknob;

    public function __construct(Doorknob $doorknob) {
        $this->doorknob = $doorknob;
    }
}

/**
 * Doorknob
 *
 * Class to describe the doorknob object.
 */
class Doorknob {

}

/**
 * HouseFactory
 *
 * Class to construct a house
 */

class HouseFactory {
    public static function makeHouse() {
        $doorknob = new Doorknob();

        //Door requires a doorknob
        $door = new Door($doorknob);

        //House requires a door, note that house DOES NOT REQUIRE A DOORKNOB!
        $house = new House($door);

        return $house;
    }
}

HouseFactory::makeHouse();