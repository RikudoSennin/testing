<?php

header("Content-type: text/plain; charset=utf8");

$array = array();

try {
    $conn = new PDO("mysql:host=localhost;dbname=rishumon", "root", 1499823);
    $conn->query("SET CHARACTER SET utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = <<<SQL
    SELECT `main`.`id`, `main`.`first_name`, `main`.`last_name`,
       `main`.`birth_date`, `main`.`sex`, `main`.`father_name`,
       `main`.`mother_name`, `countries`.`NAME_HEB` as `country`, `cities`.`NAME_CITY` as `city`
     FROM `main` ,`countries`, `cities`
     WHERE `main`.`first_name` = 'דור' AND `main`.`last_name` = 'אור'
       AND `main`.`birth_country_id` = `countries`.`ID`
       AND `main`.`city_id` = `cities`.`ID`
SQL;


    $result = $conn->query($query);

    $array = $result->fetchAll(PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
    print_r($e->errorInfo);
}


print_r($array);