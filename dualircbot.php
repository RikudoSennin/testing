<?php

set_time_limit(0);                  //Don't kill the script after a set time.
header('Content-type: text/plain'); //Easier display of the log. Maybe in the future log will look better.
error_reporting(E_ALL);             //Show any error the pops up.

/* -- Disable output buffering -- */
ob_implicit_flush(true);
ob_end_flush();
/**
 * @package None
 */

/**
 * Registry
 * This object holds the global variables, it will be passed to every object.
 *
 * @package   tests
 * @author    Truth
 * @copyright 2011
 * @access    public
 */
class Registry {
    public $vars = array();

    /**
     * Registry::__construct()
     *
     * @param Array $vars   The constructor accepts an array of variables for first use.
     *
     * @return Registry
     */
    public function __construct($vars) {
        if (is_array($vars)) {
            $this->vars = $vars;
        }
    }

    /**
     * Registry::__set()
     *
     * @param String $index
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($index, $value) {
        $this->vars[$index] = $value;
    }

    /**
     * Registry::__get()
     *
     * @param String $index
     *
     * @return mixed
     */
    public function __get($index) {
        return $this->vars[$index];
    }
}

class Bot {

    /**
     * $registry
     * @var Registry Holds the registry object
     */
    protected $registry;

    /**
     * $conn
     * @var Resource Holds the connection
     */
    protected $conn;

    /**
     * $data
     * @var String   Holds the data being read right now
     */
    protected $data;

    /**
     * $channels
     * @var array    Holds an array of Channel objects which the bot is on
     */
    protected $channels = array();

    /**
     * $users
     * @var array   Holds an array of User objects. A list of all known users to the bot
     */
    protected $users = array();

    /**
     * $me
     * @var string  Holds the bot's nickname
     */
    private $me;

    /**
     * Bot::__construct()
     * Set global variables from registry and connect.
     *
     * @param mixed $registry
     *
     * @return \Bot
     */
    public function __construct($registry) {
        $this->registry = $registry;
        $this->connect();
    }

    /**
     * Bot::putUser()
     * Add a user to the global user list found on $this->users.
     *
     * @param string $user  The nickname or full address of the user.
     * @param Channel $channel
     *
     * @return void
     */
    protected function putUser($user, $channel) {
        if (!empty($this->users[$user])) {
            return;
        }
        $matches = array();
        #Full address match
        if (preg_match("|^:?([^\s!]+)![^\s@]+@\S+$|i", $user, $matches)) {
            $this->users[$matches[1]]       = new User($matches[1], $user);
            $channel->nicklist[$matches[1]] = $this->users[$matches[1]];
            return;
        }
        #Only nickname match
        else if (preg_match("|^([^!@\s]+)$|", $user, $matches)) {
            $this->users[$matches[1]]       = new User($matches[1]);
            $channel->nicklist[$matches[1]] = $this->users[$matches[1]];
        }
        else {
            throw new Exception("Invalid username or address.");
        }
    }

    /**
     * Bot::delUser()
     * Removes a user from the bot.
     *
     * @param string $user  The user to remove.
     *
     * @return void
     */
    protected function delUser($user) {
        if (!empty($this->users[$user])) {
            $this->users[$user] = null;
        }
    }

    /**
     * Bot::debug()
     * Outputs a debug line.
     *
     * @param String $data   The data to output
     * @param String $target The target of the output (in, out)
     *
     * @return void
     */
    protected function debug($data, $target) {
        echo date('H:i:s') . " " . strtoupper($target) . ": $data \r\n";
    }

    /**
     * Bot::in()
     * Writes a line of $data onto the connection. Note the use of "\r\n" it is important!
     *
     * @param String $data  The line to write.
     *
     * @return void
     */
    protected function in($data) {
        fwrite($this->conn, $data . "\r\n");
        $this->debug($data, 'in');
    }

    /**
     * Bot::out()
     * Reads out data from the connection.
     *
     * @return void
     */
    protected function out() {
        $this->data = fgets($this->conn);
        $this->data = preg_replace("|[\r\n]|", "", $this->data);
        $this->debug($this->data, 'out');
    }

    /**
     * Bot::auth()
     * Used to authenticate a user as an administrator.
     * Will have a database connection and testing in the future.
     *
     * @param string $user
     * @param string $pass
     *
     * @return Boolean
     */
    protected function auth($user, $pass) {
        //TODO: Implement a database access system
        if (strtolower($user) == 'truth' && strtolower($pass) == '1499823') {
            return true;
        }
        return false;
    }

    /**
     * Bot::connect()
     * Connects to the IRC server and sends in basic data.
     *
     * @return void
     */
    public function connect() {
        $this->conn = fsockopen($this->registry->server, $this->registry->port, $errno, $errstr);
        if (!$this->conn) {
            throw new Exception("Connection failed! ($errno) $errstr");
        }
        if (!empty($this->registry->pass)) {
            $this->in("PASS {$this->registry->pass}");
        }
        $this->in("USER {$this->registry->mnick} 0 * :{$this->registry->rname}");
        $this->in("NICK {$this->registry->mnick}");
        $this->main();
    }

    /**
     * Bot::join()
     * Join a channel
     *
     * @param string $channel
     *
     * @return void
     */
    public function join($channel) {
        if (empty($this->channels[$channel])) {
            $this->in("JOIN :$channel");
            $this->channels[$channel] = new Channel($channel);
        }
    }

    /**
     * Bot::part()
     * Part a channel
     *
     * @param String $channel
     *
     * @return void
     */
    public function part($channel) {
        if (empty($this->channels[$channel])) {
            $this->in("PART :$channel");
            $this->channels[$channel] = null;
        }
    }

    public function msg($target, $msg) {
        $this->in("PRIVMSG $target :$msg");
    }

    /**
     * @param string $msg
     */
    public function quit($msg = "Goodbye") {
        $this->in("QUIT :$msg");
    }

    /**
     * @param User $user
     *
     * @return boolean Whether the user is an admin or not.
     */
    private function isAdmin($user) {
        return $user->getAdmin();
    }

    /**
     * @param string $fullAddress
     *
     * @return User
     */
    private function identifyUser($fullAddress) {
        $nick = explode("!", $fullAddress);
        $nick = str_replace(":", "", $nick[0]);
        return $this->users[$nick];
    }

    /**
     * Bot::main()
     * The main functions. Runs as long as there's connection.
     *
     * @return void
     */
    public function main() {
        while (!feof($this->conn)) {
            $this->out();
            $data = explode(" ", $this->data);
            #Ping pong
            if ($data[0] == "PING") {
                @$this->in("PONG $data[1]");
            }
            if ($data[1] == "005") {
                if (empty($this->me)) {
                    $this->me = $data[2];
                }
                $variables = array_slice($data, 3);
                foreach ($variables as $var) {
                    if ($var == ":are") {
                        break;
                    }
                    $array                = explode('=', $var);
                    $key                  = $array[0];
                    $value                = empty($array[1]) ? '' : $array[1];
                    $this->registry->$key = $value;
                }
            }
            #End of MOTD or MOTD missing
            if ($data[1] == "376" || $data[1] == "422") {
                $this->join("#Welcome");
            }
            if ($data[1] == "353") {
                $names   = array_slice($data, 5);
                $channel = $data[4];
                //echo "|[:{$this->registry->STATUSMSG}]|"; //FIGURE OUT WHY NOT REPLACING!!
                foreach ($names as $name) {
                    $name = preg_replace("|[:{$this->registry->STATUSMSG}]|", '', $name);
                    //echo "str_replace(|[:{$this->registry->STATUSMSG}]|, '', $name);";
                    echo $name . ' ';
                    $this->putUser($name, $this->channels[$channel]);
                    //$this->channels[$channel]->putUser($name);
                }
            }
            if ($data[1] == "366") {
                //var_dump($this->channels, $this->users, $this->registry);
            }
            if ($data[1] == "PRIVMSG") {

                $user   = $this->identifyUser($data[0]);
                $target = $data[2];
                $msg    = str_replace(":", "", array_slice($data, 3));
                //$str_msg = implode(" ", $msg);

                if ($target == $this->me) {
                    if ($msg[0] == "@auth") {
                        if (!$this->isAdmin($user)) {
                            $this->msg($user->nick, "Acknowledged. Please hold.");
                            $auth = $this->auth($msg[1], $msg[2]);
                            if ($auth) {
                                $user->setAdmin(true);
                                $this->msg($user->nick, "Authentication Succesful!");
                            }
                            else {
                                $this->msg($user->nick, "Authentication Failed. User/Password match is wrong.");
                            }
                        }
                        else {
                            $this->msg($user->nick, "You are already identified as an admin. No need to identify twice.");
                        }
                    }

                }
                if ($this->isAdmin($user)) {
                    if ($msg[0] == "@die") {
                        $this->quit("Requested by Admin - $user->nick");
                    }
                    if ($msg[0] == "@say") {
                        $this->msg($target, implode(" ", array_slice($msg, 1)));
                    }
                }

                //$this->in("PRIVMSG #Welcome :{$user->nick} said on $target: $str_msg");

            }
        }
    }
}

class Channel {

    public $name;
    public $nicklist;

    public function __construct($name) {
        if (!preg_match("/^#/", $name)) { //Channels should begin with #, if not add one.
            $name = "#$name";
        }
        $this->name = $name;
    }

    public function __get($nick) {
        return $this->nicklist[$nick];
    }

    public function __set($nick, $newnick) {
        $this->nicklist[$nick] = $newnick;
    }

    public function putUser($nick) {
        $this->nicklist[$nick] = new User($nick);
    }

    public function delUser($nick) {
        if (isset($this->nicklist[$nick])) {
            unset($this->nicklist[$nick]);
        }
    }

}

class User {
    /**
     * $nick
     * @var String holds the user's nickname
     */
    public $nick = '';

    /**
     * $fullAddress
     * @var String holds the user's full address in the form of nick!user@host.
     */
    public $fullAddress = '';

    /**
     * $admin
     * @var Boolean holds the administration status for the user.
     */
    protected $admin = false;

    public function __construct($nick, $fullAddress = '') {
        //var_dump(debug_backtrace());
        if (!empty($this->fullAddress)) {
            $this->$fullAddress = $fullAddress;
        }
        $this->nick = $nick;
    }

    public function getAdmin() {
        return $this->admin;
    }

    public function setAdmin($admin) {
        $this->admin = $admin;
    }

    public function rename($newNick) {
        $oldNick           = $this->nick;
        $this->nick        = $newNick;
        $this->fullAddress = str_replace($oldNick, $newNick, $this->fullAddress);
    }

    public function __toString() {
        return $this->nick;
    }
}

//---------------------------------
$registry = new Registry(array(

    "mnick"  => "TruthBot",
    "anick"  => "TruthBot2",
    "rname"  => "The Bot of Truth",
    "server" => "localhost",
    "port"   => 6667,
    "admin"  => array("Truth"),

));
$bot      = new Bot($registry);

?>