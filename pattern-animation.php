<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="RikudoSennin" />
	
        <title>Unique Pattern + Animation testing</title>
        
        <style>
            #pattern {
                background: url(pattern-images/red.png), url(pattern-images/blue.png), url(pattern-images/green.png);
                height: 200px;
                position: relative;
                width: 50209px;
                -webkit-animation: move 600s infinite linear;
            }
            @-webkit-keyframes move {
                0% {
                    left: 0;
                }
                100% {
                    left: -50209px;
                }
            }
        </style>
    </head>
	
    <body>
    
        <div id="pattern"></div>
    
    </body>
</html>