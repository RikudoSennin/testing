<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="author" content="Rikudo Sennin" />
        
        <meta name="viewport" content="initial-scale=1.0, userscalable=no">
        
        <style>
            html {
                height: 100%;
            }
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map_canvas {
                height: 100%;
                width: 100%;
            }
        </style>
        
        <script src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
        
        <script>
            function googleMapsInit() {
                var latlng = new google.maps.LatLng(-34.397, 150.644) //Sydney
                var myOptions = {
                    zoom: 8,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            }
        </script>
        
        <title>Google Maps Testing</title>
    </head>
	
    <body onload="googleMapsInit()">
    
        <div id="map_canvas"></div>
    
    </body>
</html>